# Aplicacion Mobile Vuelos

La aplicacion de vuelos es una aplicacion mobile en la cual se listan diferentes categorias de vuelos
junto con toda su informacion en detalle de cada uno de ellos.

## Getting Started

Estas instrucciones le permitirán obtener una copia del proyecto en funcionamiento en su máquina local para fines de desarrollo y prueba. Se parte de la suposición que la persona que clonará el repositorio trabaja con el sistema operativo macOS.

## Prerequisites

Se debe tener instalado el CLI de react native y watchman.

`$ npm i -g react-native-cli`

`$ brew install watchman`


## Installing

Pasos para poder ejecutar el proyecto:

1) Entrar a la carpeta del proyecto

`cd FLIGHTS`

2) Luego de ello ejecutar la instalación de dependencias

`npm install`

3) Finalmente se ejecuta el comando para ejecutar el observador watchman y de correr en un emulador de android

`$ npm run watchman`
`$ react-native run-ios`

## Built With

- [React](https://github.com/facebook/react/) - Librería JavaScript para construir interfaces de usuario.

- [React Native](https://github.com/facebook/react-native) - framework para construir apps nativas con React.


## Versioning

1.0.0

## Author

- **Freddy Arteaga**

## Views

### Pantalla Home

![Hola](https://preview.ibb.co/fxhPdc/home.png)

## Pantalla vuelos ida
En esta pantalla se puede buscar y seleccionar las ciudades por fecha para viajar
![enter image description here](https://preview.ibb.co/g2NCWx/screen_ida.png)

## datepicker para la seleccion de fechas
![enter image description here](https://preview.ibb.co/mPyVJc/datepicker.png)


## Pantalla Lista de vuelos

![enter image description here](https://preview.ibb.co/b7XPdc/flights_List.png)

## Pantalla detalle vuelos


![enter image description here](https://preview.ibb.co/g2JajH/detail_Flights.png)

## Pantalla vuelos ida y vuelta

![enter image description here](https://preview.ibb.co/bZYmrx/go_Back_Flights.png)

## Pantalla vuelos multidestino


![enter image description here](https://preview.ibb.co/m8rvjH/Multidestiny.png)

## License

The JavaScript Templates script is released under the [MIT license](https://opensource.org/licenses/MIT).
