import { AppRegistry } from 'react-native'
import App from './Routes'

AppRegistry.registerComponent('flights', () => App)