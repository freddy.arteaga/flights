
export default {
    navBar: {
        backgroundColor: '#174D7F',
        alignContent: 'center',
        justifyContent: 'center'
    },
    navTitle: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
        alignSelf: 'center'
    }
}
