import React, { Component } from 'react'
import { Scene, Router } from 'react-native-router-flux'
import _styles from './style'

import Tabs from '../Components/Tabs'
import Home from '../Containers/HomeFlights/content'
import Ida from '../Containers/Flights/Ida/content'
import IdaVuelta from '../Containers/Flights/IdaVuelta/content'
import Multidestino from '../Containers/Flights/Multidestino/content'
import AirportsList from '../Containers/AirportsList/content'
import FlightContent from '../Components/Flight/FlightContent'
import FligthList from '../Containers/FlightsList/content'
import FlightsDetail from '../Components/FlightsDetail'
import FligthListGoBack from '../Containers/FlightsListGoBack/content'

export default class App extends Component {
  render() {
    return (
      <Router navigationBarStyle={_styles.navBar} backTitle = ' ' titleStyle={_styles.navTitle} tintColor='#fff'>
        <Scene key='root'>
          <Scene key='home' component={Home}  title='Home' />
        <Scene key='FligthList' component={FligthList} title='FligthList' />
        <Scene key='FligthListGoBack' component={FligthListGoBack} title='FligthListGoBack' />
          <Scene key='Ida' navBar={Tabs} component={ Ida}/>
          <Scene key='IdaVuelta' navBar={Tabs} component={ IdaVuelta}/>
          <Scene key='Multidestino' navBar={Tabs} component={ Multidestino}/>
          <Scene key='AirportsList'  component={ AirportsList} title='AirportsList'/>
          <Scene key='FlightContent' navBar={Tabs} component={ FlightContent}/>
          <Scene key='FlightsDetail' component={ FlightsDetail} title='FligthDetail'/>
        </Scene>
        </Router>
    )
   }
}