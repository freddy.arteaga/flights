import { getComponentStyle } from '../../Helpers/Stylus'

export default getComponentStyle({

    container: {
        backgroundColor: 'rgba(248,248,248,1.0)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    icon: {
        fontSize: 24
    },
    text: {
        marginLeft: 37,
        marginTop: 16,
        fontSize: 20,
        fontWeight: '600',
        fontStyle: 'normal',
        lineHeight: 24,
        letterSpacing: 0,
        textAlign: 'left',
        color: '#ffffff'
    },
    tabText: {
        fontSize: 12,
        fontWeight: '600',
        fontStyle: 'normal',
        letterSpacing: 0.43,
        textAlign: 'center',
        color: '#ffffff'
    },
    contentTab: {
        width: 120,
        height: 48,
        justifyContent: 'center',
        borderLeftWidth: 0.5,
        borderLeftColor: 'white'
    }
})