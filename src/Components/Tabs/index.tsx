import React, { Component } from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import _styles from './style'
import { getComponentStyle } from '../../Helpers/Stylus'
import LinearGradient from 'react-native-linear-gradient'
import { Actions } from 'react-native-router-flux'

const stylus = getComponentStyle(_styles)

export default class Tabs extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }
    renderLeft() {
        return (
            <TouchableOpacity onPress={Actions.Ida}
                style={stylus.contentTab} >
                <Text style={stylus.tabText}>{'SOLO IDA'}</Text>
            </TouchableOpacity>
        )
    }
    renderMiddle() {
        return (
            <TouchableOpacity onPress={Actions.IdaVuelta}
                style={stylus.contentTab} >
                <Text style={stylus.tabText}>{'IDA Y VUELTA'}</Text>
            </TouchableOpacity>
        )
    }
    renderRight() {
        return (
            <TouchableOpacity onPress={Actions.Multidestino}
                style={stylus.contentTab} >
                <Text style={stylus.tabText}>{'MULTIDESTINO'}</Text>
            </TouchableOpacity>
        )
    }

    render() {
        return (

            <LinearGradient style={stylus.container} colors={['#7986AC', '#174D7F']}
                start={{ x: 0.0, y: 0.0 }} end={{ x: 0.9, y: 1.0 }}>
                <View style={{ width: 360, height: 128 }}>
                    <View style={{ width: 360, height: 24 }} />
                    <View style={{ width: 360, height: 56, flexDirection: 'row' }}>
                        <Text style={stylus.text} >{'Vuelos'}</Text>
                    </View>
                    <LinearGradient style={stylus.container} colors={['#7986AC', '#174D7F']}
                start={{ x: 0.0, y: 0.0 }} end={{ x: 0.9, y: 1.0 }}>
                    <View style={{ width: 360, height: 48, justifyContent: 'space-between', flexDirection: 'row', }}>

                        {this.renderLeft()}
                        {this.renderMiddle()}
                        {this.renderRight()}

                    </View>
                    </LinearGradient>
                </View>
            </LinearGradient>
        )
    }
}