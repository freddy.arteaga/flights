import React from 'react'
import { Text, View, TouchableOpacity, Image } from 'react-native'
import _styles from './style'
import { getComponentStyle } from '../../Helpers/Stylus'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions } from 'react-native-router-flux'

export default (props) => {

    return (

        <TouchableOpacity style={stylus.container}
            onPress={() => { Actions.FlightsDetail( {datos: props} ) }}>
            <Text style={stylus.icon}><Icon name='paper-plane' size={40} /></Text>
            <View style={stylus.titles}>
                <View style={{ flexDirection: 'column' }}>
                    <Image
                        style={{ width: 30, height: 30 }}
                        source={{ uri: props.image }} />
                </View>
                <View style={{ flexDirection: 'column', marginRight: 20 }}>
                    <Text style={stylus.textList} numberOfLine={2} ellipsizeMode={'tail'}>
                        {props.iataOrigin}</Text>
                    <Text style={stylus.textList} numberOfLine={2} ellipsizeMode={'tail'}>
                        {props.airportOrigin}</Text>
                </View>
                <View style={{ flexDirection: 'column' }}>
                    <Text style={stylus.textList} numberOfLine={2} ellipsizeMode={'tail'}>
                        {props.iataDestinition}</Text>
                    <Text style={stylus.textList} numberOfLine={2} ellipsizeMode={'tail'}>
                        {props.airportDestinition}</Text>
                </View>
                <Text style={{ fontSize: 24, fontWeight: 'bold', marginLeft: 40 }}>{props.duration}</Text>
            </View>
        </TouchableOpacity>

    )

}

const stylus = getComponentStyle(_styles)