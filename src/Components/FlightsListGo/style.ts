export default {
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 31,
        justifyContent: 'flex-start',
        height: 56,
        width: 360,
        marginTop: 50,
        borderBottomWidth: 1
    },
    text: {
        marginLeft: 35.8,
        fontSize: 16,
        lineHeight: 20,
        letterSpacing: 0,
        color: 'rgb(46, 48, 48)'
    },
    circle: {
        width: 50,
       height: 50,
       borderRadius: 25,
        alignItems: 'center',
       position: 'absolute',
       marginLeft: 180,
       opacity: 0,
       backgroundColor: 'rgba(0, 0, 0,.1)'
      },
      textList: {
        maxWidth: 223.9,
        fontSize: 16,
        color: 'rgb(46, 48, 48)',
        lineHeight: 20,
        fontWeight: 'bold',
        letterSpacing: 0,
        textAlign: 'left'
    },
    icon : {
        marginRight: 37
    },
    subText: {
        fontSize: 14,
        color: 'rgb(98, 101, 101)',
        lineHeight: 20,
        letterSpacing: 0,
        textAlign: 'left'
    },
    titles: {
        flexDirection: 'row'
    }
}