import React  from 'react'
import { Text, View, Image } from 'react-native'
import { getComponentStyle } from '../../Helpers/Stylus'
import _styles from './style'

const stylus = getComponentStyle(_styles)

export default () => {
   return (
    <View style={stylus.container}>
        <Text style={stylus.logoText}>Almundo Flights</Text>
        <Image style={stylus.image}
          source={{ 
            uri: 'https://www.ecestaticos.com/imagestatic/clipping/bf5/8e2/bf58e29a9a4f8be2e51daddfe378183a/comprar-vuelos-en-whatsapp-y-otras-formas-rapidas-y-mas-baratas-para-viajar.jpg?mtime=1491405460' }}
        />
    </View>
   )
}