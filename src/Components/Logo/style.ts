
export default {
    container: {
        justifyContent: 'center',
        alignSelf: 'center'
    },
    logoText: {
        marginTop: 35,
        marginBottom: 50,
        fontSize: 24,
        marginLeft: 15,
        fontWeight: 'bold'
    },
    image: {
        width: 200,
        height: 200,
        borderRadius: 100,
        marginBottom: 20
    }
}