import React from 'React'
import {View, TouchableOpacity, Text} from 'react-native'
import _styles from './style'
import { Actions } from 'react-native-router-flux'

export default (props) => {
    return (
        <View style={_styles.container}>
            <Text style={_styles.title}>{'Origen'}</Text>
            <TouchableOpacity style={_styles.button} onPress={() => { Actions.AirportsList() }} >
                <Text>{'Origen '}{props.d}</Text>
            </TouchableOpacity>
            <Text style={_styles.title}>{'Destino'}</Text>
            <TouchableOpacity style={_styles.button}
                onPress={() => { Actions.AirportsList() }}>
                <Text>{'Destino '}{props.destination}</Text>
            </TouchableOpacity>
        </View>
    )
}