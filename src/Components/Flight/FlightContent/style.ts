export default {
    container: {
        flexDirection: 'column',
        marginTop: 50
    },
    title: {
        fontWeight: 'bold',
        fontSize: 16
    },
    button : {
        borderBottomWidth: 1,
        borderBottomColor: 'black',
        width: 300,
        marginBottom: 20
    }
}