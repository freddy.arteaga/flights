import React from 'React'
import { View, TouchableOpacity, Text } from 'react-native'
import { getComponentStyle } from '../../../Helpers/Stylus'
import _styles from './style'
import { Actions } from 'react-native-router-flux'

const stylus = getComponentStyle(_styles)

export default () => {
    return (
        <View>
        <TouchableOpacity style={stylus.button} onPress={() => { Actions.FligthList() }}>
            <Text style={stylus.text}> {'Buscar'} </Text>
        </TouchableOpacity>
        </View>

    )
}