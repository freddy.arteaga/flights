export default {
    personCount: {
        borderBottomWidth: 1, borderBottomColor: 'black',
        width: 300, marginTop: 40
    },
    button: {
        backgroundColor: '#174D7F',
        padding: 10,
        margin: 10,
        borderRadius: 5,
        width: 250,
        marginTop: 70
      },
      text: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 20
      }
}