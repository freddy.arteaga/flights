import React from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import _styles from './style'
import { getComponentStyle } from '../../Helpers/Stylus'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions } from 'react-native-router-flux'

export default ({ iata, airport }) => {

    return (

        <TouchableOpacity style={stylus.container}
            onPress={() => { Actions.FlightsDetail({ iata, airport }) }}>
            <Text style={stylus.icon}><Icon name='plane' size={25} /></Text>
            <View style={stylus.titles}>
                <Text style={stylus.textList} numberOfLine={2} ellipsizeMode={'tail'}>
                    {iata}</Text>
                <Text style={stylus.subText}>{airport}</Text>
            </View>
        </TouchableOpacity>

    )

}

const stylus = getComponentStyle(_styles)