export default {
    imgCover: {
        width: undefined,
        height: 200
      },

      textList: {
        maxWidth: 223.9,
        fontSize: 16,
        color: 'rgb(46, 48, 48)',
        lineHeight: 20,
        fontWeight: 'bold',
        letterSpacing: 0,
        textAlign: 'left'
    }

}