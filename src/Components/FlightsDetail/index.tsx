import React from 'react'
import { Text, View, TouchableOpacity, Image } from 'react-native'
import _styles from './style'
import { getComponentStyle } from '../../Helpers/Stylus'

export default (props) => {

    return (

        <View>
            <Image style={_styles.imgCover} source=
        {{ uri: 'https://www.ecestaticos.com/imagestatic/clipping/bf5/8e2/bf58e29a9a4f8be2e51daddfe378183a/comprar-vuelos-en-whatsapp-y-otras-formas-rapidas-y-mas-baratas-para-viajar.jpg?mtime=1491405460' }} />
        <View style={{marginTop: 10, padding: 10, marginLeft: 10, marginRight: 20, flexDirection: 'column'}}>
            <Text style={stylus.textList}>{'SALIDA:  '}{props.datos.iataOrigin}</Text>
            <Text style={stylus.textList}>{'HORA SALIDA:  '}{props.datos.airportOrigin}</Text>
        </View>
        <View style={{marginTop: 10, padding: 10, marginLeft: 10, flexDirection: 'column'}}>
            <Text style={stylus.textList}>{'DURACION: '}{props.datos.duration}</Text>
            <Text style={stylus.textList}>{'HORA DE LLEGADA: '}{props.datos.airportDestinition}</Text>
        </View>
        <Text style={{ padding: 10, fontWeight: 'bold'}}>
        Recuerda que:
La información del vuelo es orientativa para el pasajero y puede sufrir modificaciones y actualizaciones por parte del aeropuerto. Por favor, consulta siempre la información de última hora en las pantallas del aeropuerto.
El horario de facturación del vuelo es el indicado en el mail de confirmación de la compra, independientemente del horario final de salida.
La información del estado de los vuelos se actualiza cada 10 minutos. Si deseas conocer el estado de un vuelo operado por otra compañía puedes hacerlo a través de su web o app.
        </Text>
        </View>

    )

}

const stylus = getComponentStyle(_styles)