import React, { Component } from 'react'

import { FlatList, View, ScrollView, ActivityIndicator } from 'react-native'
import _styles from './style'
import { getComponentStyle } from '../../Helpers/Stylus'
import ListFlights from '../../Components/FlightsList'
import { RoundTrip } from '../../Api'

const stylus = getComponentStyle(_styles)

export default class FlightsList extends Component {

  constructor(props) {
    super(props)
    this.state = {
      flightsinfo: null,
      isLoading: true
    }  }

  async componentWillMount() {
    const { iAtaOri, iAtaDes, departDate } = this.props
    console.log(iAtaOri, iAtaDes, departDate)
    let flights = await RoundTrip(iAtaDes, iAtaOri, departDate)
    this.setState({ isLoading: false, flightsinfo: flights })
  }

  render() {

    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, paddingTop: 50 }}>
          <ActivityIndicator />
        </View>
      )
    }
    return (
      <ScrollView>
        <View style={stylus.container}>
          <View style={stylus.containerRecientes} >
            <FlatList
              data={this.state.flightsinfo}
              renderItem={({ item }) => (
                <ListFlights
                  iataOrigin={item.origin.iata}
                  airportOrigin={item.origin.time}
                  iataDestinition={item.destination.iata}
                  airportDestinition={item.destination.time}
                  duration={item.duration}
                  image={item.image}
                />
              )}
              keyExtractor={item => item._id} />
          </View>
        </View>
      </ScrollView >

    )
  }
}
