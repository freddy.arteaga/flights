import React, { Component } from 'react'

import { FlatList, View, ScrollView, ActivityIndicator } from 'react-native'
import _styles from './style'
import { getComponentStyle } from '../../Helpers/Stylus'
import ListFlights from '../../Components/FlightsList'
import { RoundTrip } from '../../Api'

const stylus = getComponentStyle(_styles)

export default class FlightsList extends Component {

  constructor(props) {
    super(props)
    this.state = {
      flightsinfo: null,
      isLoading: true
    }
  }

  async componentWillMount() {
    const { text1, text2, text3 } = this.props
    let flights = await RoundTrip(text1, text2, text3)
    this.setState({ isLoading: false, flightsinfo: flights })
  }

  render() {

    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, paddingTop: 50 }}>
          <ActivityIndicator />
        </View>
      )
    }
    return (
      <ScrollView>
        <View style={stylus.container}>
          <View style={stylus.containerRecientes} >
            <FlatList
              data={this.state.flightsinfo}
              renderItem={({ item }) => (
                <ListFlights
                  airlane={item.airlane}
                  class={item.class}
                  image={item.image}
                  date={item.date}
                  iatades={item.destination.iata}
                  iataori={item.origin.iata}
                  nameOri={item.origin.name}
                  namedes={item.destination.name}
                  durtion={item.duration}
                />
              )}
              keyExtractor={item => item._id} />
          </View>
        </View>
      </ScrollView >

    )
  }
}
