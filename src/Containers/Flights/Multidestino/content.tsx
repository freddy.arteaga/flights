import React, { Component } from 'react'
import { View, DatePickerAndroid, Text, Picker, TouchableOpacity } from 'react-native'
import _styles from './style'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions } from 'react-native-router-flux'

export default class Flight extends Component {
    constructor(props) {
        super(props)
        this.state = {
            departDateOne: '',
            departDateTwo: '',
            iAtaOri: '',
            iAtaDes: ''
        }

        this.sendInfo = this.sendInfo.bind(this)
    }

        sendInfo() {
            Actions.FligthListGoBack(this.state)
        }
    
        async datePickerTwo() {
            try {
                const { action, year, month, day } = await DatePickerAndroid.open({ date: new Date() })
                if (action !== DatePickerAndroid.dismissedAction) {
                const mmonth = month + 1
                    this.setState({ departDateTwo: `${mmonth}-${day}-${year}` })
                }
            } catch ({ code, message }) {
                console.warn('Cannot open date picker', message)
            }
        }

    async datePickerOne() {
        try {
            const { action, year, month, day } = await DatePickerAndroid.open({ date: new Date() })
            if (action !== DatePickerAndroid.dismissedAction) {
                const mmonth = month + 1
                this.setState({ departDateOne: `${mmonth}-${day}-${year}` })
            }
        } catch ({ code, message }) {
            console.warn('Cannot open date picker', message)
        }
    }
    validatePicka(val, index) {
                this.setState({ iAtaOri: val })
                if (val === 0 || val === null) {
                    this.setState({ disable: true })
                } else {
                    this.setState({ disable: false })
                }
            }
        validatePickb(val, index) {
                    this.setState({ iAtaDes: val })
                    if (val === 0 || val === null) {
                        this.setState({ disable: true })
                    } else {
                        this.setState({ disable: false })
                    }
                }
    render() {

     return (
            <View style={_styles.content}>
            <View style={{flexDirection: 'row', marginTop: 40}}>
                <Picker
                    selectedValue={this.state.iAtaOri}
                    style={{ height: 50, width: 200 }}
                    onValueChange={(itemValue, itemIndex) => this.validatePicka( itemValue, itemIndex )}>
                    <Picker.Item label='Seleccione' color='rgb(23, 158, 164)' value='0' />
                    <Picker.Item label='Medellin' color='rgb(23, 158, 164)' value='MED' />
                    <Picker.Item label='Bogota' color='rgb(23, 158, 164)' value='BOG' />
                    <Picker.Item label='Cali' color='rgb(23, 158, 164)' value='CLO' />
                    <Picker.Item label='Miama' color='rgb(23, 158, 164)' value='MIA' />
                </Picker>
                <Text style={{marginTop: 20, fontWeight: 'bold'}}>Origen<Icon name='plane' size={15} /></Text>
            </View>
            <View style={{flexDirection: 'row'}}>
                <Picker
                    selectedValue={this.state.iAtaDes}
                    style={{ height: 50, width: 200 }}
                    onValueChange={(itemValue, itemIndex) => this.validatePickb( itemValue, itemIndex  )}>
                   <Picker.Item label='Seleccione' color='rgb(23, 158, 164)' value='0' />
                    <Picker.Item label='Medellin' color='rgb(23, 158, 164)' value='MED' />
                    <Picker.Item label='Bogota' color='rgb(23, 158, 164)' value='BOG' />
                    <Picker.Item label='Cali' color='rgb(23, 158, 164)' value='CLO' />
                    <Picker.Item label='Miama' color='rgb(23, 158, 164)' value='MIA' />
                </Picker>
                <Text style={{marginTop: 20, fontWeight: 'bold'}}>Destino <Icon name='plane' size={15} /></Text>
            </View>
                {/* <FlightContent d={this.props.destination} /> */}
                <View style={_styles.buttonDate}>
                    <View style={{ marginRight: 10 }}>
                        <Icon.Button name='calendar'
                            backgroundColor='#3b5998'
                            onPress={() => this.datePickerOne()} >
                            {'Fecha inicio'}
                        </Icon.Button>
                        <View><Text style={_styles.dateResut}>{this.state.departDateOne}</Text></View>
                    </View>
                    <View style={{ marginRight: 10 }}>
                    <Icon.Button name='calendar'
                    backgroundColor='#3b5998'
                    onPress={() => this.datePickerTwo()} >
                        {'Fecha fin'}
                    </Icon.Button>
                    <View><Text style={_styles.dateResut}>{this.state.departDateTwo}</Text></View>
                    </View>

                </View>
                <View>
                <Icon.Button
                    backgroundColor='#3b5998'
                    onPress={() => this.datePickerTwo()} >
                        {'Agregar Vuelos'}
                    </Icon.Button>
                </View>
                
                <TouchableOpacity style={_styles.button} onPress={this.sendInfo}>
                    <Text style={_styles.text}> {'Buscar'} </Text>
                </TouchableOpacity>
            </View>
        )
    }
}