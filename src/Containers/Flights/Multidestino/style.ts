
export default {
    content: {
        flexDirection: 'column',
        backgroundColor: 'white',
        marginTop: 10,
        height: 470,
        marginHorizontal: 10,
        shadowColor: 'black',
        shadowOpacity: .3,
        padding: 15,
        shadowOffset: {
          height: 1
        },
        elevation: 2
    },
    buttonDate : {
        flexDirection: 'row',
        marginTop: 50
    },
    dateResut: {
        fontSize: 14, fontWeight: 'bold', marginLeft: 13
    },
    button: {
        backgroundColor: '#174D7F',
        padding: 10,
        margin: 10,
        borderRadius: 5,
        width: 250,
        marginTop: 70,
        marginLeft: 20
      },
      text: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 20
      }
}