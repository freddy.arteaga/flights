import React, { Component } from 'react'

import { FlatList, View, ScrollView, ActivityIndicator } from 'react-native'
import _styles from './style'
import { getComponentStyle } from '../../Helpers/Stylus'
import AirportsList from '../../Components/AirportList'
import { getAllFligth } from '../../Api'

const stylus = getComponentStyle(_styles)

export default class FlightsList extends Component {

  constructor(props) {
    super(props)
    this.state = {
      flightsinfo: '',
      isLoading: true
    }
  }

  async componentWillMount() {
    let flights = await getAllFligth()
    this.setState({ isLoading: false, flightsinfo: flights })
}

  render() {

    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, paddingTop: 50 }}>
          <ActivityIndicator />
        </View>
      )
    }
    return (
      <ScrollView>
        <View style={stylus.container}>
          <View style={stylus.containerRecientes} >
            <FlatList
              data={this.state.flightsinfo}
              renderItem={({ item }) => (
                <AirportsList
                  iata={item.origin.iata}
                  airport={item.origin.time}
                />
              )}
              keyExtractor={item => item._id} />
          </View>
        </View>
      </ScrollView >

    )
  }
}
