import { Dimensions } from 'react-native'

export default {
  container: {
    alignItems: 'center',
    paddingHorizontal: 10,
    backgroundColor: 'rgb(248, 248, 248)',
    height: Dimensions.get('window').height

  },
  textCont: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 16,
    flexDirection: 'row'
  },
  button: {
    backgroundColor: '#174D7F',
    padding: 10,
    margin: 10,
    borderRadius: 5,
    width: 250,
    marginTop: 10
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 20
  }
}
