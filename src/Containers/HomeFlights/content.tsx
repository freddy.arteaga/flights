import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import _styles from './style'
import { getComponentStyle } from '../../Helpers/Stylus'
import Logo from '../../Components/Logo'
import { Actions } from 'react-native-router-flux'

const stylus = getComponentStyle(_styles)

export default () => {

    return (
      <View style={stylus.container}>
        <Logo />
        <View style={stylus.textCont}>
          <TouchableOpacity
            style={stylus.button}
            onPress={Actions.Ida}
          >
            <Text style={stylus.text}> {'Ingresar'} </Text></TouchableOpacity>
        </View>
      </View>

    )
  }
