export default {
    container: {
        backgroundColor: 'rgb(248, 248, 248)',
        paddingHorizontal: 17,
        height: 600,
        flexDirection: 'column'
    },
    containerBuscados: {
        borderTopWidth: 1,
        borderTopColor: 'rgba(0, 0, 0, 0.1)',
        marginTop: 19,
        backgroundColor: 'rgb(248, 248, 248)'
    },
    title: {
        marginLeft: -1,
        marginTop: 17,
        fontSize: 18,
        color: 'rgb(46, 48, 48)'
    },
    search: {
        height: 40,
        backgroundColor: 'white',
        borderColor: 'rgba(211,211,211,1)',
        borderWidth: 1,
        marginVertical: 15,
        marginHorizontal: 10,
        textAlign: 'center',
        paddingHorizontal: 10
      },
      spinner: {
        paddingTop: 20
      },
    title2: {
        marginTop: 9,
        marginBottom: 5,
        fontSize: 18,
        color: 'rgb(46, 48, 48)'
    },
    text: {
        marginTop: 22,
        marginLeft: 36,
        color: 'rgb(46, 48, 48)',
        fontSize: 16,
        lineHeight: 20,
        letterSpacing: 0,
        textAlign: 'left'
    },
    fill: {
        flexDirection: 'row',
        height: 63,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0, 0, 0, 0.1)'
    }
}