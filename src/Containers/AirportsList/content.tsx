import React, { Component } from 'react'

import { FlatList, View, Text, ScrollView, TextInput, ActivityIndicator } from 'react-native'
import _styles from './style'
import { getComponentStyle } from '../../Helpers/Stylus'
import List from '../../Components/AirportList'
import { getAllFligth } from '../../Api'

const stylus = getComponentStyle(_styles)

export default class AirportsList extends Component {

  constructor(props) {
    super(props)
    this.state = {
      flightsinfo: null,
      isLoading: true
    }
  }

  async componentWillMount() {
    let flights = await getAllFligth()
    this.setState({ isLoading: false, flightsinfo: flights })
  }

  render() {

    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, paddingTop: 50 }}>
          <ActivityIndicator />
        </View>
      )
    }
    return (
      <ScrollView>
        <View style={stylus.container}>
          <TextInput
            placeholder='Search' />
          <View style={stylus.containerRecientes} >
            <Text style={stylus.title}>{'Recientes'}</Text>
            <FlatList
              data={this.state.flightsinfo}
              renderItem={({ item }) => (
                <List
                  iata={item.origin.iata}
                  airport={item.origin.airport}
                />
              )}
              keyExtractor={item => item._id} />
          </View>
        </View>
      </ScrollView >

    )
  }
}
